# MemoryReport

A Xojo desktop project that displays a Xojo applications memory usage
and object count growth over time, to aid in detecting memory leaks.

## Background

This particular app was developed to test a suspected line of Xojo code that was
possibly causing memory leaks. (in Xojo 2021r2.1 and possibly thru 2021r1)
Its likely to not show this particular leak in subsequent Xojo releases.
(see bug report: 64642 which Xojo reports as now fixed)


It proved so useful that I now use a pared-down version that I add in to other
projects so I can monitor the app for leaks at any time. 


 ![MainWindow](Images/MemoryReportMainWindow.png)


## Standalone Code Tester

1. Downlaod and build the project using the Xojo IDE

1. Run the program and:
	1. Press Rerun Report as often as you like to establish inital values and stable usage
	1. Press start/stop timer to run the report once per second
	1. (optional) Place some of your own suspect code in method codeToTest()
	1. Press Run1000Tests button to run your suspected code.


## Pare-down and add to other projects

This is where this demonstration project is most helpful IMHO.

For version of the window you can add to other projects follow these approximate 
modifications in your own project.

- remove the Timer1 control 
- remove the btnRunTimer button
- remove the btnRun1000Tests button
- remove lblTimerStatus label
- (keep the btnRunReport)

Then
- add that window into the target Xojo Project in the IDE
- also add the ObjectStatusItem class to that Xojo Project in the IDE
- add a menubar item (or other control) that will show the window

## Credits

Credit is due to Kem Tekinay on the Xojo Forums at:
[https://forum.xojo.com/t/finding-memory-leak/32719/4](https://forum.xojo.com/t/finding-memory-leak/32719/4)

He provided the basic idea and core code prototype that iterates
over objects and counts memory items.   

## License
MIT  (see the LICENSE file included)




